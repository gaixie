#!/bin/sh
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# -----------------------------------------------------------------------------
PRG="$0"

while [ -h "$PRG" ]; do
    ls=`ls -ld "$PRG"`
    link=`expr "$ls" : '.*-> \(.*\)$'`
    if expr "$link" : '/.*' > /dev/null; then
	PRG="$link"
    else
	PRG=`dirname "$PRG"`/"$link"
    fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`

# Only set GAIXIE_HOME if not already set
[ -z "$GAIXIE_HOME" ] && GAIXIE_HOME=`cd "$PRGDIR/.." ; pwd`

if [ -z "$JAVA_HOME" -a -z "$JRE_HOME" ]; then
    echo "Neither the JAVA_HOME nor the JRE_HOME environment variable is defined"
    echo "At least one of these environment variable is needed to run this program"
    exit 1
fi

if [ -z "$JRE_HOME" ]; then
    JRE_HOME="$JAVA_HOME"
fi

_RUNJAVA="$JRE_HOME"/bin/java

if [ -r "$GAIXIE_HOME"/conf/config.properties ]; then
    GAIXIE_CONFIG="file:$GAIXIE_HOME/conf/config.properties"
fi

for i in "$GAIXIE_HOME"/bin/*.jar; do
    CLASSPATH="$CLASSPATH":"$i"
done

"$_RUNJAVA" -Dgaixie.home="$GAIXIE_HOME" \
    -Dfelix.config.properties="$GAIXIE_CONFIG" \
    -classpath "$CLASSPATH" org.apache.felix.main.Main "$@"
