/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.security.ui.internal;

import java.io.IOException;
import java.util.Calendar;
import java.util.Dictionary;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletOutputStream;

import org.gaixie.security.ui.internal.utils.ServletHtmlUtils;
import org.gaixie.service.security.AuthorityService;
import org.gaixie.service.security.model.Authority;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;

/**
 * Public API representing an Security service
 */
public class MainServlet extends HttpServlet {
    private AuthorityService authService = null;

    public synchronized void setAuthorityService(AuthorityService authService){
        this.authService = authService;
    }

    public synchronized void unsetAuthorityService(AuthorityService authService){
        if(this.authService == authService)
	    this.authService = null;
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
        throws IOException {
	//	System.out.println("mainServlet="+req.getSession().getServletContext());
	//	System.out.println(req.getSession().getServletContext().getAttribute("test"));
        resp.setContentType("text/html");
	List<Authority> auths = authService.findByUsername("rsit-user2");
        ServletOutputStream output=resp.getOutputStream();

	output.println(ServletHtmlUtils.headWithTitle("Gaixie Main Page")+
		       ServletHtmlUtils.css("/security/security.css")+
		       ServletHtmlUtils.javascript("/security/jquery.js")+
		       ServletHtmlUtils.javascript("/security/json2.js")+
		       ServletHtmlUtils.javascript("/security/main.js")+
		       //ServletHtmlUtils.header(req)+
		       ServletHtmlUtils.headerWithAuth(req,auths)+
		       ServletHtmlUtils.message()+
		       content()+
		       ServletHtmlUtils.footer());
        output.close();
    }

    public void doPost(HttpServletRequest req,  HttpServletResponse resp)
        throws IOException{
        doGet(req, resp);
    }

    private String content() {
        StringBuilder sb = new StringBuilder();
        sb.append("<table class=\"plaintable\">\n" +
		  "  <thead>\n"+
		  "    <tr>\n"+
		  "      <th>Bundle Id</th>\n"+
		  "      <th>Bundle Name</th>\n"+
		  "      <th>Bundle Vendor</th>\n"+
		  "      <th>Bundle Version</th>\n"+
		  "      <th>Bundle Description</th>\n"+
		  "      <th>Bundle LastModified</th>\n"+
		  "      <th>Bundle State</th>\n"+
		  "    </tr>\n"+
		  "  </thead>\n"+
		  "  <tbody>\n");
	BundleContext bc = SecurityActivator.bc;
	Bundle[] bundles = bc.getBundles();
	for (Bundle bundle :bundles) {
	    Calendar cal = Calendar.getInstance();
	    cal.setTimeInMillis(bundle.getLastModified());
	    Dictionary headers = bundle.getHeaders();
	    sb.append( "    <tr>\n"+
		       "      <td>"+bundle.getBundleId()+"</td>\n"+
		       "      <td>"+(String)headers.get(Constants.BUNDLE_NAME)+"</td>\n"+
		       "      <td>"+(String)headers.get(Constants.BUNDLE_VENDOR)+"</td>\n"+
		       "      <td>"+(String)headers.get(Constants.BUNDLE_VERSION)+"</td>\n"+
		       "      <td>"+(String)headers.get(Constants.BUNDLE_DESCRIPTION)+"</td>\n"+
		       "      <td>"+cal.getTime()+"</td>\n");
	    int state = bundle.getState(); 
	    switch (state) {
	    case Bundle.UNINSTALLED: 
		sb.append("      <td>UNINSTALLED</td>\n"); break;
	    case Bundle.INSTALLED: 
		sb.append("      <td>INSTALLED</td>\n"); break;
	    case Bundle.RESOLVED: 
		sb.append("      <td>RESOLVED</td>\n"); break;
	    case Bundle.STARTING: 
		sb.append("      <td>STARTING</td>\n"); break;
	    case Bundle.STOPPING: 
		sb.append("      <td>STOPPING</td>\n"); break;
	    case Bundle.ACTIVE: 
		sb.append("      <td>ACTIVE</td>\n"); break;
	    }
	    sb.append("    </tr>\n");
	}
	sb.append("  </tbody>\n");
	sb.append("</table>\n");
	return sb.toString();
    }
}

