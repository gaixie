/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.gaixie.security.ui.internal.utils;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;

import org.gaixie.service.security.model.Authority;

public class ServletHtmlUtils {
    public static final String DOCTYPE =
        "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">";
    public static String headWithTitle(String title) {
        return(DOCTYPE + "\n" +
               "<html>\n" +
               "<head><title>" + title + "</title>\n" +
               "  <meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\" />\n");
    }

    public static String css(String path) {
        return("  <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\""+path+"\" />\n");
    }

    public static String javascript(String path) {
        return("  <script type=\"text/javascript\" src=\""+path+"\"></script>\n");
    }

    public static String header(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        boolean show = true;
        String username =null;
        if (null!=session) {
            username = (String)session.getValue("username");
            if (null != username) {
                show = false;
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("</head>\n"+
                  "<body>\n"+
                  "  <div id=\"header\">\n"+
                  "    <div id=\"welcome\" style='display:"+((show)? "none":"block")+"'>"+username+" | <a href=\"/login.w?t=logout\">Logout</a></div>\n"+
                  "  </div>\n");
        return sb.toString();
    }

    public static String headerWithAuth(HttpServletRequest req, List<Authority> auths) {
        HttpSession session = req.getSession(false);
        boolean show = true;
        String username =null;
        if (null!=session) {
            username = (String)session.getValue("username");
            if (null != username) {
                show = false;
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("</head>\n"+
                  "<body>\n"+
                  "  <div id=\"header\">\n"+
                  "    <div id=\"welcome\" style='display:"+((show)? "none":"block")+"'>"+username+" | <a href=\"/login.w?t=logout\">Logout</a></div>\n");

	sb.append("      <ul id=\"nav\">\n");
	sb.append("        <li><a href=\"#\">Gaixie</a></li>\n");
	if (null == auths) sb.append("</ul>\n");
	/* pre:  前一个节点的层数
	 * cur:  当前节点的层数
	 * dist: 上一个节点和当前节点的层距
         * auths结果集的根节点层数为0
	 */
	int pre=1;
	for (Authority auth : auths) {
	    int cur = auth.getDepth();
	    int dist = pre-cur;
	    // 判断是否是叶子节点，通过span做界面标记
	    String name = (auth.getRgt()-auth.getLft()==1)? auth.getName():auth.getName()+"    >";
	    // 根节点不做处理
	    if (cur == 0) continue;
	    if (dist < 0) {  // 层数在增加
		sb.append("<ul>\n<li><a href=\""+auth.getValue()+"\">"+name+"</a>\n");
	    } else if (dist > 0) { // 层数在减少
		for (int i=0;i<dist;i++) {
		    sb.append("</li>\n</ul>\n");
		}
		sb.append("</li>\n<li><a href=\""+auth.getValue()+"\">"+name+"</a>\n");
	    } else{ // 层数没变化
		sb.append("<li><a href=\""+auth.getValue()+"\">"+name+"</a>\n");
	    }
	    pre = cur;
	}
	// 最后一个节点处理完后，对所有打开的ul和li进行封闭
	for (int i=0;i<pre;i++) {
	    sb.append("</li>\n</ul>\n");
	}

	sb.append("      </div>\n");
        return sb.toString();
    }

    public static String menu(HttpSession session) {
	return null;
    }

    public static String message() {
        return("  <center><div id=\"message\"></div></center>\n");
    }

    public static String footer() {
        return("</body>\n"+
               "</html>");
    }

}