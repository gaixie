/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.security.ui.internal;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletOutputStream;

import org.gaixie.security.ui.internal.utils.ServletHtmlUtils;

/**
 * Public API representing an Security service
 */
public class IndexServlet extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
        throws IOException {
        resp.setContentType("text/html");
        ServletOutputStream output=resp.getOutputStream();
        HttpSession session = req.getSession(false);
 
	output.println(ServletHtmlUtils.headWithTitle("Welcome Page")+
		       ServletHtmlUtils.css("/security/security.css")+
		       ServletHtmlUtils.javascript("/security/jquery.js")+
		       ServletHtmlUtils.javascript("/security/json2.js")+
		       ServletHtmlUtils.javascript("/security/login.js")+
		       ServletHtmlUtils.header(req)+
		       ServletHtmlUtils.message()+
		       content(req)+
		       ServletHtmlUtils.footer());
        output.close();
    }

    public void doPost(HttpServletRequest req,  HttpServletResponse resp)
        throws IOException{
        doGet(req, resp);
    }

    private String content(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        boolean show = true;
        String username =null;
        if (null!=session) {
            username = (String)session.getValue("username");
            if (null != username) {
                show = false;
            }
        }

        StringBuilder sb = new StringBuilder();
        sb.append("  <form id=\"login_form\" method=\"post\" action=\"javascript:\" style='display:"+((show)? "block":"none")+"'>\n"+
		  "    <p>\n"+
		  "      Username<br/>\n"+
		  "      <input id=\"user_name\" type=\"text\" value=\"\" name=\"username\"/><br/>\n"+
		  "    </p>\n"+
		  "    <p>\n"+
		  "      Password<br/>\n"+
		  "      <input id=\"user_pass\" type=\"password\" value=\"\" name=\"password\"/><br/>\n"+
		  "    </p>\n"+
		  "    <input id=\"login_button\" type=\"submit\" value=\"Login\" />\n"+
		  "    <div id=\"forgot_pass\"><a title=\"Password Lost and Found\" href=\"#\">Forgot password?</a></div>\n"+
		  "  </form>\n");
        return sb.toString();
    }
}