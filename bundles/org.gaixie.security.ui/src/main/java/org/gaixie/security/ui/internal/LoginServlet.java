/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.security.ui.internal;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import org.gaixie.json.JSONException;
import org.gaixie.json.JSONObject;
import org.gaixie.service.security.LoginService;

/**
 * 
 */
public class LoginServlet extends HttpServlet {
    private LoginService loginService = null;

    public synchronized void setLoginService(LoginService loginService){
        this.loginService = loginService;
    }

    public synchronized void unsetLoginService(LoginService loginService){
        if(this.loginService == loginService)
	    this.loginService = null;
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
        throws IOException {
	String type = req.getParameter("t");
	if("login".equals(type))
	    login(req,resp);
	else
	    logout(req,resp);
    }

    public void doPost(HttpServletRequest req,  HttpServletResponse resp)
        throws IOException{
        doGet(req, resp);
    }

    private void login(HttpServletRequest req, HttpServletResponse resp) 
        throws IOException {
	Map<String, Object> jsonMap = new HashMap<String, Object>();
        String username=req.getParameter("username");
        String password=req.getParameter("password");
        ServletOutputStream output=resp.getOutputStream();

        try {
	    if(loginService==null){
		jsonMap.put("success", false);
		jsonMap.put("message", "No usable login service");
		return;
	    }

            loginService.login(username, password);
	    // check if we have a session
	    HttpSession ses = req.getSession(true);
	    ses.putValue("username", username);

	    //resp.sendRedirect("/main.y");
	    jsonMap.put("success", true);
	    jsonMap.put("data", username);
        } catch (Exception e) {
	    jsonMap.put("success", false);
	    jsonMap.put("message", "Login Failed! "+e.getMessage());
        } finally {
            output.println((new JSONObject(jsonMap)).toString());
            output.close();
	}
    }

    private void logout(HttpServletRequest req,  HttpServletResponse resp)
        throws IOException{
	HttpSession ses = req.getSession(true);
	ses.invalidate(); // make servlet engine forget the session
	System.out.println(req.getServletPath());
	resp.sendRedirect("/");    
    }

}

