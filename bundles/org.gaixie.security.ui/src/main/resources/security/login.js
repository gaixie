// When DOM is ready
$(document).ready(function(){
	$("#login_form").submit(function(){
		//var str = $(this).serialize();
		var username = $('#user_name');
		var password = $('#user_pass');
		var postdata = 'username=' + username.val() + 
		    '&password=' + password.val()+
		    '&t=login';
		$.ajax({  
			type: "POST",
			    url: "/login.w",  // Send the login info to this page
			    data: postdata,  
			    success: function(rst){ 
			    var str = rst;
			    var obj = JSON.parse(str); 
			    if(obj.success==true){
				$("#message").css("color","green");
				window.location = 'main.y';
			    }  
			    else{  
				$("#message").css("color","red");
			    }
			    var response = obj.message;
			    if(response){
				$('#message').html(response);
			    }
			}
  
		    });  
	    });
    });
