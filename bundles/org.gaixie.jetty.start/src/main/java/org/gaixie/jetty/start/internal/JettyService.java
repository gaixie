/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaixie.jetty.start.internal;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Properties;

import org.gaixie.http.base.DispatcherServlet;
import org.gaixie.http.base.HttpServiceController;
import org.mortbay.jetty.Connector;
import org.mortbay.jetty.nio.SelectChannelConnector;
import org.mortbay.jetty.security.HashUserRealm;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.servlet.Context;
import org.mortbay.jetty.servlet.ServletHolder;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.framework.BundleContext;

public final class JettyService implements Runnable {
    /** PID for configuration of the HTTP service. */
    private static final String PID = "org.gaixie.http";

    private final BundleContext context;
    private boolean running;
    private Thread thread;
    private Server server;
    private DispatcherServlet dispatcher;
    private final HttpServiceController controller;

    public JettyService(BundleContext context, DispatcherServlet dispatcher, HttpServiceController controller) {
        this.context = context;
        this.dispatcher = dispatcher;
        this.controller = controller;
    }
    
    public void start() throws Exception {
        this.thread = new Thread(this, "Jetty HTTP Service");
        this.thread.start();
    }

    public void stop() throws Exception {
        this.running = false;
        this.thread.interrupt();

        try {
            this.thread.join(3000);
        } catch (InterruptedException e) {
            // Do nothing
        }
    }

    private void startJetty() {
        try {
	    this.server = new Server();

	    Connector connector = new SelectChannelConnector();
	    connector.setPort(8091);
	    connector.setMaxIdleTime(60000);
	    this.server.addConnector(connector);

	    Context context = new Context(this.server, "/", Context.SESSIONS);
	    context.addServlet(new ServletHolder(this.dispatcher), "/*");

	    this.server.start();
        } catch (Exception e) {
            System.out.println("Exception while initializing Jetty.");
        }
    }

    private void stopJetty() {
        try {
            this.server.stop();
        } catch (Exception e) {
            System.out.println("Exception while stopping Jetty.");
        }
    }

    public void run() {
        this.running = true;
        Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());

        while (this.running) {

	    try {
		Thread.sleep( 1000 ); // simple polling for now
	    }
	    catch( InterruptedException e ) {
            }

            startJetty();

            synchronized (this) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    // we will definitely be interrupted
                }
            }

            stopJetty();
        }
    }
}
