/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.service.security;

import java.util.List;

import org.gaixie.service.security.model.Authority;
import org.gaixie.service.security.SecurityException;

/**
 * Public API representing an authority service
 */
public interface AuthorityService {

    Authority get(String name);

    void addChild(Authority auth, Authority pAuth) throws SecurityException;

    List<Authority> getAll();

    List<Authority> findByUsername(String username);

    boolean verify(String value, String username);
}
