/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.service.security;

import java.util.List;

import org.gaixie.service.security.model.Authority;
import org.gaixie.service.security.model.Role;
import org.gaixie.service.security.model.User;
import org.gaixie.service.security.SecurityException;

/**
 * Public API representing an role service
 */
public interface RoleService {

    Role get(String name);

    void addChild(Role auth, Role parent) throws SecurityException;

    List<Role> getAll();

    void bind(Role role, Authority auth) throws SecurityException;

    List<Role> findByAuthority(Authority auth);

    void bind(Role role, User user) throws SecurityException;

    List<Role> findByUser(User user);

    List<String> findByAuthvalue(String authvalue);

    List<String> findByUsername(String username);

}
