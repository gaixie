/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.security.internal;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.dbutils.DbUtils;
import org.gaixie.commons.cache.Cache;
import org.gaixie.commons.cache.CacheUtils;
import org.gaixie.commons.db.ConnectionUtils;
import org.gaixie.security.internal.dao.AuthorityDAO;
import org.gaixie.service.security.SecurityException;
import org.gaixie.service.security.AuthorityService;
import org.gaixie.service.security.RoleService;
import org.gaixie.service.security.model.Authority;
import org.gaixie.service.security.model.Role;
import org.gaixie.service.security.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of Authority service
 */
public class AuthorityServiceImpl implements AuthorityService {
    Logger logger = LoggerFactory.getLogger(AuthorityServiceImpl.class);

    public Authority get(String name) {
        Connection conn = null;
	Authority auth = null;
        try {
            conn = ConnectionUtils.getConnection();
            AuthorityDAO authDAO = new AuthorityDAO(conn);
            auth = authDAO.get(name);
        } catch(SQLException e) {
	    logger.error("get authority sub tree error: ",e);
        } finally {
            DbUtils.closeQuietly(conn);
        }
	return auth;
    }

    public void addChild(Authority auth, Authority parent) throws SecurityException {
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();
            AuthorityDAO authDAO = new AuthorityDAO(conn);
	    // get latest value like lft, rgt
	    parent = authDAO.get(parent.getName());
	    if (null == parent) {
		throw new SecurityException("Parent authority had been updated by other one.");
	    }
	    authDAO.saveChild(auth,parent);
            DbUtils.commitAndClose(conn);
	    Cache cache = CacheUtils.getAuthCache();
	    cache.remove("authorities.all");
        } catch(SQLException e) {
            DbUtils.rollbackAndCloseQuietly(conn);
	    if (e.getMessage().indexOf("constraint") != -1)
		throw new SecurityException("The authority existed.");
        } 
    }

    public List<Authority> getAll() {
        Connection conn = null;
	List<Authority> auths = null;

	Cache cache = CacheUtils.getAuthCache();
	auths = (List<Authority>)cache.get("authorities.all");
	if (null != auths) return auths;

        try {
            conn = ConnectionUtils.getConnection();
            AuthorityDAO authDAO = new AuthorityDAO(conn);
            auths = authDAO.getAll();
	    cache.put("authorities.all",auths);
        } catch(SQLException e) {
	    logger.error("get authority tree error: ",e);
        } finally {
            DbUtils.closeQuietly(conn);
        }
	return auths;
    }

    public List<Authority> findByUsername(String username) {
	List<Authority> matchedAuths = new ArrayList<Authority>();
	List<Authority> auths = getAll();
	RoleService rs = new RoleServiceImpl();
	List<String> userRoles = rs.findByUsername(username);
	for (Authority auth : auths) {
	    // auth.value如果以.x结尾，不应该显示在菜单上，例如一个单纯响应ajax的servlet
	    if (auth.getValue().endsWith(".x")) continue;
	    // auth.value如果是#，直接添加
	    if (auth.getValue().equals("#")) {
		matchedAuths.add(auth);
		continue;
	    }
	    
	    List<String> authRoles = rs.findByAuthvalue(auth.getValue());
	    // 如果authRoles为空，表示此auth没有权限控制，任何用户都可以访问
	    if (null == authRoles) {
		matchedAuths.add(auth);
		continue;
	    }
	    
	    for (String role : authRoles) {
		if (userRoles.contains(role)) {
		    matchedAuths.add(auth);
		    break;
		}
	    }
	}

	/*
	 * 对结果集进行一次遍历，删除空的中间节点。
	 * 由于数据结构的特殊性，需要依靠ListIterator从后往前进行迭代。
	 */
	Authority pre = null;
	long lft=0,rgt=0;
	ListIterator litr = matchedAuths.listIterator(matchedAuths.size());
	while(litr.hasPrevious()) {
	    pre = (Authority)litr.previous();
	    // 判断是否新分支，第一个处理的node也认为是一个新分支
	    boolean newBranch = (lft > pre.getLft() && rgt > pre.getRgt()) || (lft == 0);
	    boolean midNode = "#".equals(pre.getValue());
	    // 如果新分支以一个中间节点开始，删除它(记住这里是倒着向前迭代)
	    if (newBranch && midNode ) {
		litr.remove();
	    } else {
		lft = pre.getLft();
		rgt = pre.getRgt();
	    }
	}
	return matchedAuths;
    }


    public boolean verify(String value, String username) {
	RoleService rs = new RoleServiceImpl();
	List<String> authRoles = rs.findByAuthvalue(value);
	// 如果authRoles为空，表示此auth没有权限控制，任何用户都可以访问
	if (null == authRoles) return true;
	List<String> userRoles = rs.findByUsername(username); 
	for (String role : authRoles) {
	    if (userRoles.contains(role)) {
		return true;
	    }
	}
	return false;
    }
}
