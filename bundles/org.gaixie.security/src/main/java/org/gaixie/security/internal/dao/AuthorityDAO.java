/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.security.internal.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.gaixie.service.security.model.Authority;

/**
 * Implementation of Authority DAO
 */
public class AuthorityDAO {
    private Connection conn = null;
    private QueryRunner run = null;

    public AuthorityDAO(Connection conn) {
	this.conn = conn;
	this.run = new QueryRunner();
    }

    public Authority get(String name) throws SQLException {
	ResultSetHandler<Authority> h = new BeanHandler(Authority.class);
	return run.query(conn
			 , "SELECT id, name, value, lft, rgt FROM authorities WHERE name=? "
			 , h
			 , name); 
    }

    public void saveChild(Authority auth,Authority pAuth) throws SQLException {
	if (null==pAuth) return ;

	run.update(conn
		   , "UPDATE authorities set lft=lft+2 where lft > ?"
		   , pAuth.getLft()); 

	run.update(conn
		   , "UPDATE authorities set rgt=rgt+2 where rgt > ?"
		   , pAuth.getLft()); 

	run.update(conn
		   , "INSERT INTO authorities (name,value,lft,rgt) values (?,?,?,?)"
		   , auth.getName()
		   , auth.getValue()
		   , pAuth.getLft()+1
		   , pAuth.getLft()+2); 

    }

    public List<Authority> getAll() throws SQLException {
	ResultSetHandler<List<Authority>> h = new BeanListHandler(Authority.class);
	return  run.query(conn
			  ,"SELECT node.id, node.name, node.value, node.lft, node.rgt, (COUNT(parent.name)-1) AS depth "+
			  " FROM authorities AS node, authorities AS parent "+
              " WHERE node.lft BETWEEN parent.lft AND parent.rgt "+
			  " GROUP BY node.id, node.name, node.value, node.lft, node.rgt "+
			  " ORDER BY node.lft"
			  , h);
    }
}
