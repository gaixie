/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.security.internal;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;
import org.gaixie.commons.db.ConnectionUtils;
import org.gaixie.security.internal.dao.TableDAO;
import org.gaixie.service.db.DBException;
import org.gaixie.service.db.TableExistedException;
import org.gaixie.service.db.TableService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of Table service
 */
public class TableServiceImpl implements TableService {
    Logger logger = LoggerFactory.getLogger(TableServiceImpl.class);

    public void create(String dbType) throws DBException {
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();
	    TableDAO tableDAO = new TableDAO(conn);
	    if (tableDAO.isExistedByName("USERBASE")) 
		throw new TableExistedException("Couldn't create tables");
	    tableDAO.executeLineByLine(dbType);
            DbUtils.commitAndClose(conn);
        } catch(SQLException e) {
	    logger.error("Error executing: ",e);
            DbUtils.rollbackAndCloseQuietly(conn);
        } catch(IOException e) {
	    logger.error("read script file error",e);
	} finally {
            DbUtils.closeQuietly(conn);
        }
    }

    public boolean isExistedByName(String tableName) {
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();
	    TableDAO tableDAO = new TableDAO(conn);
	    return tableDAO.isExistedByName(tableName); 
        } catch(SQLException e) {
	    logger.error("check table is existed error",e);
	} finally {
            DbUtils.closeQuietly(conn);
        }
	return false;
    }
}
