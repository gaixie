/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.security.internal;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;
import org.gaixie.commons.db.ConnectionUtils;
import org.gaixie.security.internal.dao.UserDAO;
import org.gaixie.service.security.SecurityException;
import org.gaixie.service.security.UserNotFoundException;
import org.gaixie.service.security.UserExistedException;
import org.gaixie.service.security.UserService;
import org.gaixie.service.security.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of User service
 */
public class UserServiceImpl implements UserService {
    Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    public User get(String username) {
        Connection conn = null;
	User user = null;
        try {
            conn = ConnectionUtils.getConnection();
            UserDAO userDAO = new UserDAO(conn);
            user = userDAO.get(username);
        } catch(SQLException e) {
	    logger.error("get user error: ",e);
        } finally {
            DbUtils.closeQuietly(conn);
        }
	return user;
    }

    public void add(User user) throws SecurityException {
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();
            UserDAO userDAO = new UserDAO(conn);
            userDAO.save(user);
            DbUtils.commitAndClose(conn);
        } catch(SQLException e) {
            DbUtils.rollbackAndCloseQuietly(conn);
            throw new SecurityException("Couldn't add the user", e);
        }
    }
}
