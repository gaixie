/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.security.internal;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.gaixie.commons.cache.Cache;
import org.gaixie.commons.cache.CacheUtils;
import org.gaixie.commons.db.ConnectionUtils;
import org.gaixie.security.internal.dao.AuthorityDAO;
import org.gaixie.security.internal.dao.RoleDAO;
import org.gaixie.security.internal.dao.UserDAO;
import org.gaixie.service.security.SecurityException;
import org.gaixie.service.security.RoleService;
import org.gaixie.service.security.model.Authority;
import org.gaixie.service.security.model.Role;
import org.gaixie.service.security.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of Role service
 */
public class RoleServiceImpl implements RoleService {
    Logger logger = LoggerFactory.getLogger(RoleServiceImpl.class);

    public Role get(String name) {
        Connection conn = null;
	Role role = null;
        try {
            conn = ConnectionUtils.getConnection();
            RoleDAO roleDAO = new RoleDAO(conn);
            role = roleDAO.get(name);
        } catch(SQLException e) {
	    logger.error("get role tree error: ",e);
        } finally {
            DbUtils.closeQuietly(conn);
        }
	return role;
    }

    public void addChild(Role role, Role parent) throws SecurityException {
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();
            RoleDAO roleDAO = new RoleDAO(conn);
	    // get latest value like lft, rgt
	    parent = roleDAO.get(parent.getName());
	    if (null == parent) {
		throw new SecurityException("Parent role had been updated by other one.");
	    }
	    roleDAO.saveChild(role,parent);
            DbUtils.commitAndClose(conn);
        } catch(SQLException e) {
            DbUtils.rollbackAndCloseQuietly(conn);
	    if (e.getMessage().indexOf("constraint") != -1)
		throw new SecurityException("The role existed.");
        } 
    }

    public List<Role> getAll() {
        Connection conn = null;
	List<Role> roles = null;
        try {
            conn = ConnectionUtils.getConnection();
            RoleDAO roleDAO = new RoleDAO(conn);
            roles = roleDAO.getAll();
        } catch(SQLException e) {
	    logger.error("get role tree error: ",e);
        } finally {
            DbUtils.closeQuietly(conn);
        }
	return roles;
    }

    public void bind(Role role, Authority auth) throws SecurityException {
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();
            RoleDAO roleDAO = new RoleDAO(conn);
	    // 绑定前现同步一下数据库，防止并发修改
	    // 而且直接new对象，id可能为空
	    role = roleDAO.get(role.getName());
	    AuthorityDAO authDAO = new AuthorityDAO(conn);
	    auth = authDAO.get(auth.getName());
	    if (null == role || null == auth) {
		throw new SecurityException("Role or Authority had been updated by other one.");
	    } else if ("#".equals(auth.getValue())) {
		throw new SecurityException("The Authority cant bind any role.");
	    }
	    roleDAO.bind(role,auth);
            DbUtils.commitAndClose(conn);
	    // 从cache中删除此auth对应的roles
	    // 下次调用findByAuthority的时候装载
	    Cache cache = CacheUtils.getAuthCache();
	    cache.remove(auth.getValue());
        } catch(SQLException e) {
            DbUtils.rollbackAndCloseQuietly(conn);
	    throw new SecurityException("Bind authority failed.");
        } 
    }

    public List<Role> findByAuthority(Authority auth) {
	// authority的value如果为#，只用于显示tree的中间节点
	// 不应该有任何权限
	String key = auth.getValue();
	if ("#".equals(key)) return null;

        Connection conn = null;
	List<Role> roles = null;

        try {
            conn = ConnectionUtils.getConnection();
            RoleDAO roleDAO = new RoleDAO(conn);
	    // 同步数据库，防止并发修该，同时防止auth.id为空
	    AuthorityDAO authDAO = new AuthorityDAO(conn);
	    auth = authDAO.get(auth.getName());

	    if (null == auth) return null;
            roles = roleDAO.findByAuthority(auth);

        } catch(SQLException e) {
	    logger.error("find role sub tree error: ",e);
        } finally {
            DbUtils.closeQuietly(conn);
        }
	return roles;

    }

    public void bind(Role role, User user) throws SecurityException {
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();
            RoleDAO roleDAO = new RoleDAO(conn);
	    // 绑定前现同步一下数据库，防止并发修改
	    role = roleDAO.get(role.getName());
	    UserDAO userDAO = new UserDAO(conn);
	    user = userDAO.get(user.getUsername());
	    if (null == role || null == user) {
		throw new SecurityException("Role or User had been updated by other one.");
	    }
	    roleDAO.bind(role,user);
            DbUtils.commitAndClose(conn);
	    // 从cache中删除此user对应的roles
	    // 下次调用findByUser的时候装载
	    Cache cache = CacheUtils.getUserCache();
	    cache.remove(user.getUsername());
        } catch(SQLException e) {
            DbUtils.rollbackAndCloseQuietly(conn);
	    throw new SecurityException("Bind user failed.");
        } 
    }

    public List<Role> findByUser(User user) {
        Connection conn = null;
	List<Role> roles = null;
        try {
            conn = ConnectionUtils.getConnection();
	    UserDAO userDAO = new UserDAO(conn);
	    user = userDAO.get(user.getUsername());
	    if (null == user) return null;

            RoleDAO roleDAO = new RoleDAO(conn);
            roles = roleDAO.findByUser(user);
        } catch(SQLException e) {
	    logger.error("find role list tree error: ",e);
        } finally {
            DbUtils.closeQuietly(conn);
        }
	return roles;

    }


    public List<String> findByAuthvalue(String value) {
        Connection conn = null;
	Cache cache = CacheUtils.getAuthCache();
	List<String> names = (List<String>)cache.get(value);
	if (null != names) return names;

        try {
            conn = ConnectionUtils.getConnection();
            RoleDAO roleDAO = new RoleDAO(conn);
            names = roleDAO.findByAuthvalue(value);
	    cache.put(value,names);
        } catch(SQLException e) {
	    logger.error("find role sub tree error: ",e);
        } finally {
            DbUtils.closeQuietly(conn);
        }
	return names;

    }

    public List<String> findByUsername(String username) {
        Connection conn = null;
	Cache cache = CacheUtils.getUserCache();
	List<String> names = (List<String>)cache.get(username);
	if (null != names) return names;

        try {
            conn = ConnectionUtils.getConnection();
            RoleDAO roleDAO = new RoleDAO(conn);
            names = roleDAO.findByUsername(username);
	    cache.put(username,names);
        } catch(SQLException e) {
	    logger.error("find role list tree error: ",e);
        } finally {
            DbUtils.closeQuietly(conn);
        }
	return names;

    }
}
