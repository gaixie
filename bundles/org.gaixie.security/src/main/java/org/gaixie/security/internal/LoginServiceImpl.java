/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.security.internal;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;
import org.gaixie.commons.db.ConnectionUtils;
import org.gaixie.security.internal.dao.UserDAO;
import org.gaixie.service.security.LoginService;
import org.gaixie.service.security.SecurityException;
import org.gaixie.service.security.UserNotFoundException;
import org.gaixie.service.security.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of Login service
 */
public class LoginServiceImpl implements LoginService {
    Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

    public void login(String username, String password) throws SecurityException {
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();
	    UserDAO userDAO = new UserDAO(conn);
	    User user = userDAO.get(username);
	    if (null == user) throw new UserNotFoundException("User not found");
        } catch(SQLException e) {
	    throw new SecurityException("Please confirm that database configration is correct.");
        } finally {
            DbUtils.closeQuietly(conn);
	    // userDAO = null; ?
        }
    }
}
