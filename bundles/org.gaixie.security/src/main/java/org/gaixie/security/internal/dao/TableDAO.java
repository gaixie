/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.security.internal.dao;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;

/**
 * Implementation of User DAO
 */
public class TableDAO {
    private Connection conn = null;

    public TableDAO(Connection conn) {
        this.conn = conn;
    }

    public void executeLineByLine(String dbType) throws SQLException, IOException {
	StringBuilder command = new StringBuilder();
        InputStream is = this.getClass().getResourceAsStream("/dbscripts/"+dbType+"/createdb.sql");
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	QueryRunner run = new QueryRunner();
	String line;
	while ((line = reader.readLine()) != null) {
	    command = handleLine(command, line, run);
	}
        reader.close();
	checkForMissingLineTerminator(command);
    }

    private StringBuilder handleLine(StringBuilder command 
				    ,String line
				    ,QueryRunner run) throws SQLException {
	String trimmedLine = line.trim();
	if (isComment(trimmedLine)) {
	    //System.out.println(command);
	} else if (trimmedLine.endsWith(";")) {
	    command.append(line.substring(0, line.lastIndexOf(";")));
	    command.append(" ");
	    //System.out.println(command);
	    run.update(conn, command.toString()); 
	    command.setLength(0);
	} else if (trimmedLine.length() > 0) {
	    command.append(line);
	    command.append(" \n");
	}
	return command;
    }

    private boolean isComment(String trimmedLine) {
	return trimmedLine.startsWith("//") || trimmedLine.startsWith("--");
    }

    /*
     * 如果有未执行的sql，抛出SQLException，强制在service中rollback
     */
    private void checkForMissingLineTerminator(StringBuilder command) 
	throws SQLException {
	if (command != null && command.toString().trim().length() > 0) {
	    throw new SQLException("Missing end-of-line terminator (;) => " + command);
	}
    }

    public boolean isExistedByName(String tableName) throws SQLException {
        DatabaseMetaData dbm = this.conn.getMetaData();
        ResultSet rs = dbm.getTables(null, null, tableName, null);
        if (rs.next()) return true;
        return false;
    }
}
