/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.security.internal;

import org.gaixie.service.db.DBException;
import org.gaixie.service.db.TableExistedException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TableServiceImplTest {
    private TableServiceImpl service;

    @Before public void init() {
	service = new TableServiceImpl();
    }


    @Test public void testCreate() throws DBException {
	if (!service.isExistedByName("USERBASE")) {
	    service.create("derby");
	}
    }

    @Test (expected=TableExistedException.class)  
	public void testCreateExistedTable() throws DBException {
	if (service.isExistedByName("USERBASE")) {
		service.create("derby");
	}
    }

}
