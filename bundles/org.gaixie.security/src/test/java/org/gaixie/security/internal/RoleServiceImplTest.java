/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.security.internal;

import java.util.List;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.gaixie.service.db.DBException;
import org.gaixie.service.security.SecurityException;
import org.gaixie.service.security.model.Authority;
import org.gaixie.service.security.model.Role;
import org.gaixie.service.security.model.User;

public class RoleServiceImplTest {
    private RoleServiceImpl service;
    private AuthorityServiceImpl authServ;
    private UserServiceImpl userServ;
    @Before public void init() {
        service = new RoleServiceImpl();
	authServ = new AuthorityServiceImpl();
	userServ = new UserServiceImpl();
        TableServiceImpl ts = new TableServiceImpl();
	try {ts.create("derby");}
	catch(DBException e) {}

    }

    @Test public void testAddChild() throws SecurityException {
	Role root = service.get("ROLE_BASE");
	int rgtOld = root.getRgt();
        service.addChild(new Role("rsit-role","only read for security modules")
			 ,root);
	service.addChild(new Role("rsit-role-1","all permissions of security")
			 ,service.get("rsit-role"));
	root = service.get("ROLE_BASE");
	int rgtNew = root.getRgt();
	assertTrue(rgtNew==(rgtOld+4));
	int descendants = (rgtNew - root.getLft() -1)/2; 
	assertTrue(descendants==2);
    }

    @Test (expected=SecurityException.class) 
        public void testAddExistedRole() throws SecurityException {
        service.addChild(new Role("rsit-rolex","only read for security modules")
			 ,service.get("ROLE_BASE"));
        service.addChild(new Role("rsit-rolex","only read for security modules")
			 ,service.get("ROLE_BASE"));
    }

    @Test public void testGetAll() {
        List<Role> roles = service.getAll();
	assertNotNull(roles);
    }

    @Test public void testBindAuthority() throws SecurityException {
	authServ.addChild(new Authority("rsit-authbind","rsit-authbind.x")
			  ,new Authority("Gaixie","#"));
        service.addChild(new Role("rsit-rolebind","x")
			 ,service.get("ROLE_BASE"));
	service.bind(new Role("rsit-rolebind","x")
		     ,new Authority("rsit-authbind","rsit-authbind.x"));
    }

    @Test (expected=SecurityException.class) 
        public void testBindNotExistedAuthority() throws SecurityException {
	service.bind(new Role("ROLE_BASE","x")
		     ,new Authority("rsit-auth-null","rsit-auth-null.x"));
    }

    @Test public void testFindByAuthority() throws SecurityException {
	authServ.addChild(new Authority("rsit-findbyauth","rsit-findbyauth.x")
			  ,new Authority("Gaixie","#"));
        service.addChild(new Role("rsit-rolefindbyauth","x")
			 ,service.get("ROLE_BASE"));
	service.bind(new Role("rsit-rolefindbyauth","x")
		     ,new Authority("rsit-findbyauth","rsit-findbyauth.x"));
	List<Role> roles = service.findByAuthority(new Authority("rsit-findbyauth"
								 ,"rsit-findbyauth.x"));
	int size =roles.size();
	assertNotNull(roles);
	assertTrue(size==1);
	List<String> list = service.findByAuthvalue("rsit-findbyauth.x");	
	assertTrue(size == list.size());
	roles = service.findByAuthority(new Authority("rsit-auth-null","rsit-auth-null.x"));
	assertNull(roles);
    }

    @Test public void testBindUser() throws SecurityException {
	User user = new User("rsit-userbind","rsit-user","123456","",true);
	userServ.add(user);
        service.addChild(new Role("rsit-rolebinduser","x")
			 ,service.get("ROLE_BASE"));
	service.bind(new Role("rsit-rolebinduser","only read for security modules")
		     ,user);
    }

    @Test (expected=SecurityException.class) 
        public void testBindNotExistedUser() throws SecurityException {
	Role node = service.get("ROLE_BASE");
	User user = new User("rsit-user-null","rsit-user-null","123456","",true);
	service.bind(node ,user);
    }

    @Test public void testFindByUser() throws SecurityException {
	User user = new User("rsit-userfindby","rsit-userfindby","123456","",true);
	userServ.add(user);
        service.addChild(new Role("rsit-rolefindbyuser","x")
			 ,service.get("ROLE_BASE"));
	service.bind(new Role("rsit-rolefindbyuser","x") ,user);
	List<Role> roles = service.findByUser(user);
	int size = roles.size();
	assertNotNull(roles);
	// 继承了ROLE_BASE
	assertTrue(size==2);
	List<String> list = service.findByUsername("rsit-userfindby");	
	assertTrue(size == list.size());
    }

    // 关于authority验证的重负荷测试，此用例耗时较长，不用每次都执行。
    //@Test 
	public void testPerformence() throws SecurityException {
	Role root = service.get("ROLE_BASE");
        service.addChild(new Role("rsit-role-pf","only read for security modules")
			 ,root);
	service.addChild(new Role("rsit-role-pf-1","all permissions of security")
			 ,service.get("rsit-role-pf"));

	for (int i=0;i<500;i++){
	    authServ.addChild(new Authority("rsit-auth-tp-l2-"+i,"#")
			     ,new Authority("Gaixie","#"));
	    Authority auth = new Authority("rsit-auth-tp-l3-"+i,"/rsit-auth-tp-l3-"+i+".v");
	    authServ.addChild(auth, new Authority("rsit-auth-tp-l2-"+i,"#"));

	    // service.bind(new Role("ROLE_BASE",""),auth);
	    service.bind(new Role("rsit-role-pf",""),auth);
	    service.bind(new Role("rsit-role-pf-1",""),auth);
	}

	UserServiceImpl userServ = new UserServiceImpl();
        User user = new User("rsit-user-pf","rsit-user-pf","123456","",true);
        userServ.add(user);
	service.bind(new Role("rsit-role-pf-1","x"),user);

	// Get menu first time , no cache
	long start = System.currentTimeMillis();
	authServ.findByUsername("rsit-user-pf");
	System.out.println("no cache:" + (System.currentTimeMillis() - start) +" ms");
	// get menu second time , in cache
	start = System.currentTimeMillis();
	authServ.findByUsername("rsit-user-pf");
	System.out.println("in cache:" + (System.currentTimeMillis() - start) +" ms");

	// verify , in cache
	start = System.nanoTime();
	authServ.verify("/rsit-auth-tp-l3-100.v","rsit-user-pf");
	System.out.println("in cache:" + (System.nanoTime() - start) +" ns");
    }
}
