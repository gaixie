/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.security.internal;

import java.util.List;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.gaixie.service.db.DBException;
import org.gaixie.service.security.SecurityException;
import org.gaixie.service.security.model.Authority;
import org.gaixie.service.security.model.Role;
import org.gaixie.service.security.model.User;

public class AuthorityServiceImplTest {
    private AuthorityServiceImpl service;
    private RoleServiceImpl roleServ;
    private UserServiceImpl userServ;

    @Before public void setup() {
        service = new AuthorityServiceImpl();
	roleServ = new RoleServiceImpl();
	userServ = new UserServiceImpl();
        TableServiceImpl ts = new TableServiceImpl();
	try { ts.create("derby");} 
	catch(DBException e) {}

    }


    @Test public void testAddChild() throws SecurityException {
        service.addChild(new Authority("asit-authadd","#")
			 ,new Authority("Gaixie","#"));
	Authority auth = service.get("asit-authadd");
	int rgtOld = auth.getRgt();
	service.addChild(new Authority("asit-authadd-1","/asit-authadd-1.x"),auth);
	auth = service.get("asit-authadd");
	int rgtNew = auth.getRgt();
	assertTrue(rgtNew==(rgtOld+2));
	int descendants = (rgtNew - auth.getLft() -1)/2; 
	assertTrue(descendants==1);
    }

    @Test (expected=SecurityException.class) 
        public void testAddExistedAuthority() throws SecurityException {
        service.addChild(new Authority("Gaixie","#")
			 ,service.get("Gaixie"));
    }

    @Test public void testGetAll() throws SecurityException {
        List<Authority> auths = service.getAll();
	assertNotNull(auths);
    }
    
    @Test public void testFindByUser() throws SecurityException {
	// 初始化Authoirty的结构
	// Gaixie (#)
	//   asit-auth (#)
	//     asit-auth-5 (/asit-auth-5.v)
	//     asit-auth-4 (#)
	//     asit-auth-3 (/asit-auth-3.v)
	//     asit-auth-2 (#)
	//       asit-auth-2-1 (#)
	//     asit-auth-1 (/asit-auth-1.x)
        service.addChild(new Authority("asit-auth","#"),new Authority("Gaixie","#"));
	Authority auth = service.get("asit-auth");
	service.addChild(new Authority("asit-auth-5","/asit-auth-5.v"),auth);
	service.addChild(new Authority("asit-auth-4","#"),auth);
	service.addChild(new Authority("asit-auth-3","/asit-auth-3.v"),auth);
	service.addChild(new Authority("asit-auth-2","#"),auth);
	service.addChild(new Authority("asit-auth-2-1","#"),new Authority("asit-auth-2","#"));
	service.addChild(new Authority("asit-auth-1","/asit-auth-1.x"),auth);

	// 初始化Role的结构
	// ROLE_BASE
	//   asit-role-1
	//     asit-role-1-1
	//   asit-role-2
        roleServ.addChild(new Role("asit-role-1","x")
			  ,new Role("ROLE_BASE","x"));
        roleServ.addChild(new Role("asit-role-2","x")
			  ,new Role("ROLE_BASE","x"));
	roleServ.addChild(new Role("asit-role-1-1","x")
			  ,new Role("asit-role-1","x"));

	// 绑定Role与Authority
	// Gaixie (#)
	//   asit-auth (#)
	//     asit-auth-5 (/asit-auth-5.v)  <--> asit-role-2
	//     asit-auth-4 (#)
	//     asit-auth-3 (/asit-auth-3.v)  <--> asit-role-1
	//     asit-auth-2 (#)
	//       asit-auth-2-1 (#)
	//     asit-auth-1 (/asit-auth-1.x)  <--> asit-role-1
	roleServ.bind(new Role("asit-role-1","x")
		      ,new Authority("asit-auth-3","/asit-auth-3.v"));
	roleServ.bind(new Role("asit-role-1","x")
		      ,new Authority("asit-auth-1","/asit-auth-1.x"));

	roleServ.bind(new Role("asit-role-2","x")
		      ,new Authority("asit-auth-5","/asit-auth-5.v"));

	// 绑定Role与User
	// ROLE_BASE
	//   asit-role-1
	//     asit-role-1-1    <--> asit-user-1
	//   asit-role-2
	// 此用户拥有了3个角色的权限
	// (ROLE_BASE --> asit-role-1 --> asit-role-1-1)
        User user = new User("asit-user-1","asit-user-1","123456","",true);
        userServ.add(user);
	roleServ.bind(new Role("asit-role-1-1","x")
		      ,new User("asit-user-1","asit-user-1","123456","",true));

        List<Authority> auths = service.findByUsername("asit-user-1");
	/*
	 * 得到的菜单权限集合为：
	 * Gaixie
	 *   asit-auth
	 *     asit-auth-3
	 * 原有的axit-auth-1.x 是一个非菜单节点，被过滤掉了。
	 */
	assertTrue(auths.size()==3);
    }

    @Test public void testVerify() throws Exception {
	// 初始化Authoirty的结构
	// Gaixie (#)
	//   asit-authvf1 (/asit-authvf1.x)
	//   asit-authvf2 (/asit-authvf2.x)
	//   asit-authvf3 (/asit-authvf3.x)
        service.addChild(new Authority("asit-authvf1","/asit-authvf1.x")
			 ,new Authority("Gaixie","#"));
        service.addChild(new Authority("asit-authvf2","/asit-authvf2.x")
			 ,new Authority("Gaixie","#"));
        service.addChild(new Authority("asit-authvf3","/asit-authvf3.x")
			 ,new Authority("Gaixie","#"));

	// 初始化Role的结构
	// ROLE_BASE
	//   asit-rolevf
        roleServ.addChild(new Role("asit-rolevf","x")
			  ,new Role("ROLE_BASE","x"));

	// 绑定Role与Authority
	// Gaixie (#)
	//   asit-authvf1 (/asit-authvf1.x)  <--> ROLE_BASE
	//   asit-authvf2 (/asit-authvf2.x)  <--> asit-rolevf
	//   asit-authvf3 (/asit-authvf3.x)
	roleServ.bind(new Role("ROLE_BASE","x")
		      ,new Authority("asit-authvf1","/asit-authvf1.x"));
	roleServ.bind(new Role("asit-rolevf","x")
		      ,new Authority("asit-authvf2","/asit-authvf2.x"));

        userServ.add(new User("asit-uservf1","asit-uservf1","123456","",true));
        userServ.add(new User("asit-uservf2","asit-uservf2","123456","",true));

	// 绑定Role与User
	// ROLE_BASE        <--> asit-uservf1
	//   asit-rolevf    <--> asit-uservf2
	roleServ.bind(new Role("ROLE_BASE","x")
		      ,new User("asit-uservf1","asit-uservf1","123456","",true));
	roleServ.bind(new Role("asit-rolevf","x")
		      ,new User("asit-uservf2","asit-uservf2","123456","",true));

	boolean result;
	// asit-uservf2通过角色继承获得ROLE_BASE的权限
	result = service.verify("/asit-authvf1.x","asit-uservf2");
	assertTrue(result);
	// asit-uservf2也有当前角色拥有的权限
	result = service.verify("/asit-authvf2.x","asit-uservf2");
	assertTrue(result);
	result = service.verify("/asit-authvf2.x","asit-uservf1");
	assertFalse(result);
	// asit-authvf3.x 没有绑定任何角色，所有用户都可以访问
	result = service.verify("/asit-authvf3.x","asit-uservf1");
	assertTrue(result);

    }

}
