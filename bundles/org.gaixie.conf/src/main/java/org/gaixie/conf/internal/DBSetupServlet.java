/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.conf.internal;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Properties;
import java.util.Map;
import java.util.Iterator;


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import org.gaixie.service.db.DBException;
import org.gaixie.service.db.TableService;
import org.gaixie.service.security.AuthorityService;
import org.gaixie.service.security.RoleService;
import org.gaixie.service.security.SecurityException;
import org.gaixie.service.security.UserService;
import org.gaixie.service.security.model.Authority;
import org.gaixie.service.security.model.Role;
import org.gaixie.service.security.model.User;

/**
 * 
 */
public class DBSetupServlet extends HttpServlet {
    public static final String DOCTYPE =
        "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">";

    private TableService tableService = null;
    private UserService userService = null;
    private RoleService roleService = null;
    private AuthorityService authService = null;


    public synchronized void setTableService(TableService tableService){
        this.tableService = tableService;
    }
    public synchronized void unsetTableService(TableService tableService){
        if(this.tableService == tableService)
	    this.tableService = null;
    }

    public synchronized void setUserService(UserService userService){
        this.userService = userService;
    }
    public synchronized void unsetUserService(UserService userService){
        if(this.userService == userService)
	    this.userService = null;
    }

    public synchronized void setRoleService(RoleService roleService){
        this.roleService = roleService;
    }
    public synchronized void unsetRoleService(RoleService roleService){
        if(this.roleService == roleService)
	    this.roleService = null;
    }

    public synchronized void setAuthorityService(AuthorityService authService){
        this.authService = authService;
    }
    public synchronized void unsetAuthorityService(AuthorityService authService){
        if(this.authService == authService)
	    this.authService = null;
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
        throws IOException {

	String type = req.getParameter("t");
	if("start".equals(type))
	    startSetup(req,resp);
	else
	    beforeSetup(req,resp);


    }

    public void doPost(HttpServletRequest req,  HttpServletResponse resp)
        throws IOException{
        doGet(req, resp);
    }

    private void beforeSetup(HttpServletRequest req,  HttpServletResponse resp)
        throws IOException{

        resp.setContentType("text/html");
        ServletOutputStream output=resp.getOutputStream();
 
	output.println(headWithTitle("Gaixie Setup Page")+
		       header(req)+
		       content(req)+
		       footer());
        output.close();

    }

    private String headWithTitle(String title) {
        return(DOCTYPE + "\n" +
               "<html>\n" +
               "<head><title>" + title + "</title>\n" +
               "  <meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\" />\n");
    }

    private String css(String path) {
        return("  <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\""+path+"\" />\n");
    }

    private String javascript(String path) {
        return("  <script type=\"text/javascript\" src=\""+path+"\"></script>\n");
    }

    private String header(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        boolean show = true;
        String username =null;
        if (null!=session) {
            username = (String)session.getValue("username");
            if (null != username) {
                show = false;
            }
        }
        StringBuffer sb = new StringBuffer();
        sb.append("</head>\n"+
                  "<body>\n"+
                  "  <div id=\"header\">\n"+
                  "    <div id=\"welcome\" style='display:"+((show)? "none":"block")+"'>"+username+" | <a href=\"/login.w?t=logout\">Logout</a></div>\n"+
                  "    <div id=\"header_nav\">\n"+
	          "    </div>\n"+
                  "  </div>\n");
        return sb.toString();
    }

    private void startSetup(HttpServletRequest req,  HttpServletResponse resp)
        throws IOException{

        resp.setContentType("text/html");
        ServletOutputStream output=resp.getOutputStream();
	output.println(headWithTitle("Gaixie Setup Page")+
		       header(req));
	String message=null;

	try {
	    tableService.create("derby");
	    User user = new User("administrator","admin","123456","bitorb@gmail.com",true);
	    userService.add(user);
	    testPerformence();
	    message = "<div style=\"color:green\">Success!</div>";
	} catch(Exception e) {
	    e.printStackTrace();
	    message = "<div style=\"color:red\">Failed! "+e.getMessage()+"</div>";
	} finally {
	    output.println("  <center>"+message+"</center>\n");
	    output.println(footer());
	    output.close();
	}
    }


    private String content(HttpServletRequest req) {
        StringBuilder sb = new StringBuilder();
        sb.append("  <center>\n"+
		  "    <p>\n"+
		  "      Click the \"Setup\" button to start installation.\n"+
		  "    </p>\n"+
		  "    <form method=\"post\" action=\"/admin/setup.w?t=start\">\n"+
		  "    <input type=\"submit\" value=\"Setup\" />\n"+
		  "    </form>\n"+
		  "  </center>\n");
        return sb.toString();
    }

    private String message() {
        return("  <center><div id=\"message\"></div></center>\n");
    }

    private String footer() {
        return("</body>\n"+
               "</html>");
    }

    private void testPerformence() throws SecurityException {
	if (roleService == null )
	    System.out.println("roleService is null");
	Role root = roleService.get("ROLE_BASE");
        roleService.addChild(new Role("rsit-role","only read for security modules")
			     ,root);
	roleService.addChild(new Role("rsit-role-1","all permissions of security")
			     ,roleService.get("rsit-role"));

	authService.addChild(new Authority("System","#")
			     ,new Authority("Gaixie","#"));

	authService.addChild(new Authority("CRM","#")
			     ,new Authority("Gaixie","#"));

	for (int i=0;i<20;i++){
	    authService.addChild(new Authority("System-l2-"+i,"#")
				 ,new Authority("System","#"));
	    for (int j=0;j<10;j++){
		authService.addChild(new Authority("System-l3-"+i+"-"+j,"#")
				     ,new Authority("System-l2-"+i,"#"));
		authService.addChild(new Authority("System-l4-"+i+"-"+j,"#")
				     ,new Authority("System-l3-"+i+"-"+j,"#")
				     );

		Authority auth = new Authority("System-l5-"+i+"-"+j,"/system-l5-"+i+"-"+j+".v");
		authService.addChild(auth, new Authority("System-l4-"+i+"-"+j,"#"));

		// service.bind(new Role("ROLE_BASE",""),auth);
		roleService.bind(new Role("rsit-role",""),auth);
		roleService.bind(new Role("rsit-role-1",""),auth);
	    }

	}

	for (int i=0;i<20;i++){
	    authService.addChild(new Authority("CRM-l2-"+i,"#")
				 ,new Authority("CRM","#"));
	    authService.addChild(new Authority("CRM-l3-"+i,"#")
				 ,new Authority("CRM-l2-"+i,"#")
				 );

	    Authority auth = new Authority("CRM-l4-"+i,"/crm-l4-"+i+".v");
	    authService.addChild(auth, new Authority("CRM-l3-"+i,"#"));

	    // service.bind(new Role("ROLE_BASE",""),auth);
	    roleService.bind(new Role("rsit-role",""),auth);
	    roleService.bind(new Role("rsit-role-1",""),auth);
	}

        User user = new User("rsit-user2","rsit-user2","123456","",true);
        userService.add(user);
	roleService.bind(new Role("rsit-role-1","x"),user);
    }
}
