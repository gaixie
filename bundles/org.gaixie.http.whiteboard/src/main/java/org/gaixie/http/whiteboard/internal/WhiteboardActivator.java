/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaixie.http.whiteboard.internal;

import java.util.ArrayList;

import org.gaixie.http.whiteboard.internal.manager.ExtenderManager;
import org.gaixie.http.whiteboard.internal.manager.ExtenderManagerImpl;
import org.gaixie.http.whiteboard.internal.tracker.FilterTracker;
import org.gaixie.http.whiteboard.internal.tracker.HttpContextTracker;
import org.gaixie.http.whiteboard.internal.tracker.HttpServiceTracker;
import org.gaixie.http.whiteboard.internal.tracker.ServletTracker;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;


public final class WhiteboardActivator implements BundleActivator {
    private final ArrayList<ServiceTracker> trackers;
    private ExtenderManager manager;

    public WhiteboardActivator() {
        this.trackers = new ArrayList<ServiceTracker>();
    }

    public void start(BundleContext bc) throws Exception {
        this.manager = new ExtenderManagerImpl();
        addTracker(new HttpContextTracker(bc, this.manager));
        addTracker(new FilterTracker(bc, this.manager));
        addTracker(new ServletTracker(bc, this.manager));
        addTracker(new HttpServiceTracker(bc, this.manager));
    }

    public void stop( BundleContext bc ) throws Exception {
        for (ServiceTracker tracker : this.trackers) {
            tracker.close();
        }

        this.trackers.clear();
        this.manager.unregisterAll();
    }

    private void addTracker(ServiceTracker tracker) {
        this.trackers.add(tracker);
        tracker.open();
    }
}
