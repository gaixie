/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.gaixie.commons.db;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.derby.jdbc.EmbeddedDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ConnectionUtils {
    private static final Logger logger = LoggerFactory.getLogger(ConnectionUtils.class);
    // 通过maven-bundle-plugin自动生成Import-package，避免生成bundle时提示package没有被使用的警告
    private static final String fDriverClassName = org.apache.derby.jdbc.EmbeddedDriver.class.getName();
    private static final String fConnectURI = "jdbc:derby:memory:gaixiedb;create=true";
    private static final String fUsername = "";
    private static final String fPassword = "";
    private static DataSource sDataSource=null;

    private static DataSource setupDataSource() {
        BasicDataSource bds = new BasicDataSource();
        bds.setDriverClassName(fDriverClassName);
        bds.setUsername(fUsername);
        bds.setPassword(fPassword);
        bds.setUrl(fConnectURI);
        return bds;
    }

    public static synchronized DataSource getDataSource() {
        if(sDataSource != null){
	    return sDataSource;
	} 
	sDataSource = setupDataSource();
        return sDataSource;
    }

    public static Connection getConnection() throws SQLException {
        Connection conn=null;
        ClassLoader oldLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(ConnectionUtils.class.getClassLoader());
            conn = getDataSource().getConnection();
	    conn.setAutoCommit(false);
        } finally {
            Thread.currentThread().setContextClassLoader(oldLoader);
        }
        return conn;
    }

    public static void printDataSourceStats(DataSource ds) throws SQLException {
        BasicDataSource bds = (BasicDataSource) ds;
        logger.debug("NumActive = " + bds.getNumActive()+" ; NumIdle = " + bds.getNumIdle());
    }
}
